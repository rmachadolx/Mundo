﻿

namespace Mundo
{
    using Servicos;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    public partial class Continente : Form
    {
        private List<Pais> Paises;
        private DataService dataService;
        public Continente()
        {
            InitializeComponent();
            Paises = new List<Pais>();
            dataService = new DataService();
            Paises = dataService.GetDataPaises();
        }

        private void Continente_Load(object sender, EventArgs e)
        {
            int numero = 0;
            string regiao = "";
            string country = "";


            var listaContinentes = Paises.Select(x => x.region).Distinct();

            foreach (var continente in listaContinentes)
            {
                if (continente != String.Empty)
                {
                    TreeView1.Nodes.Add(continente);
                    regiao = continente;

                    var listaPaises = from pais in Paises
                                      select pais;


                    int numero1 = 0;
                    foreach (Pais nacao in listaPaises)
                    {
                        country = nacao.name;
                        if (nacao.region == regiao)

                        {
                            TreeView1.Nodes[numero].Nodes.Add(nacao.name);
                            foreach (var item in Paises)
                            {
                           
                          
                                if (country == item.name && regiao == item.region)
                                {
                                    TreeView1.Nodes[numero].Nodes[numero1].Nodes.Add("Capital: " + Convert.ToString(item.capital));
                                    TreeView1.Nodes[numero].Nodes[numero1].Nodes.Add("População: " + String.Format("{0:N0} habitantes", item.population));
                                    TreeView1.Nodes[numero].Nodes[numero1].Nodes.Add("Área: " + String.Format("{0:N0} km quadrados", item.area));
                                    TreeView1.Nodes[numero].Nodes[numero1].Nodes.Add("Gini: " + String.Format("{0:N1} *", item.gini));
                                    double densidade = Math.Round(Convert.ToDouble(Convert.ToDouble(item.population) / item.area), 1);
                                    TreeView1.Nodes[numero].Nodes[numero1].Nodes.Add("Densidade populacional: " + densidade.ToString());
                                    TreeView1.Nodes[numero].Nodes[numero1].Nodes.Add("Moeda");
                                    TreeView1.Nodes[numero].Nodes[numero1].Nodes.Add("Dominio de topo");
                                    foreach (var moeda in item.currencies)
                                    {
                                       
                                            TreeView1.Nodes[numero].Nodes[numero1].Nodes[5].Nodes.Add("Nome: " + moeda.name);
                                            TreeView1.Nodes[numero].Nodes[numero1].Nodes[5].Nodes.Add("Código: " + moeda.code);
                                            TreeView1.Nodes[numero].Nodes[numero1].Nodes[5].Nodes.Add("Simbolo: " + moeda.symbol);
                                      


                                    }
                                    foreach (var dominio in item.topLevelDomain)
                                    {
                                        TreeView1.Nodes[numero].Nodes[numero1].Nodes[6].Nodes.Add(dominio.ToString());

                                    }
                                }
                            }
                            numero1++;
                        }
                    }
                    numero++;
                }
            }          
        }

    }
}


