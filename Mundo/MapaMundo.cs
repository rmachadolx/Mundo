﻿

namespace Mundo
{
    using GMap.NET;
    using GMap.NET.WindowsForms;
    using GMap.NET.WindowsForms.Markers;
    using GMap.NET.MapProviders;
    using System.Windows.Forms;
    using Mundo;
    using System.Collections.Generic;
    using System;
    using Mundo.Servicos;
    using Mundo.Modelos;

    public partial class MapaMundo : Form
    {

        List<Pais> paises = new List<Pais>();
        Pais pais = new Pais();

        GMap.NET.WindowsForms.GMapOverlay markers = new GMap.NET.WindowsForms.GMapOverlay("markers");
        public MapaMundo(List<Pais> Paises)
        {

            InitializeComponent();
            paises = Paises;

            ComboBoxPaises.DataSource = paises;
            ComboBoxPaises.DisplayMember = "name";
            ControlMapa.MapProvider = GMap.NET.MapProviders.BingHybridMapProvider.Instance;
             GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerOnly;


            ControlMapa.ShowCenter = false;
            LabelInfo.Text = "Posicione o rato no marcador 'pin' vermelho para obter as coordenadas geográficas!";
            CarregarListViewMoedas();
            CarregarListViewDominios();

        }

        private void CarregarListViewDominios()
        {
            ListViewDominios.Columns.Add("Dominios");
        }

        private void CarregarListViewMoedas()
        {
            ListViewMoedas.Columns.Add("Código");
            ListViewMoedas.Columns.Add("Simbolo");
            ListViewMoedas.Columns.Add("Nome");

        }


        private void ComboBoxPaises_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            pais = (Pais)ComboBoxPaises.SelectedItem;
            markers.Clear();
            if (pais.latlng.Count != 0)
            {

                GMapMarker marker =
                  new GMarkerGoogle(new PointLatLng(Convert.ToDouble(pais.latlng[0]), Convert.ToDouble(pais.latlng[1])), GMarkerGoogleType.red_pushpin);
                marker.ToolTipText = "\n" + pais.name + "\n" + "\nLatitude:" + pais.latlng[0] + "\nLongitude:" + pais.latlng[1];
                markers.Markers.Add(marker);
                ControlMapa.Overlays.Add(markers);
                richTextBox1.Text = String.Format("Capital: " + pais.capital + "\nRegião: " + pais.region + "\nPopulação: " + String.Format("{0:N0} habitantes", pais.population) + "\nÁrea: " + String.Format("{0:N0} km quadrados", pais.area) + "\nGini: " + String.Format("{0:N1} *", pais.gini));
                DisplayCoins(pais);
                DisplayDomains(pais);


            }
            else
            {
                markers.Clear();
            }
        }

        private void DisplayDomains(Pais pais)

        {
            ListViewDominios.Items.Clear();
            foreach (var dominio in pais.topLevelDomain)
            {
                ListViewItem item;
                item = ListViewDominios.Items.Add(dominio);
                item.SubItems.Add(dominio);
            }
            for (int i = 0; i <= ListViewDominios.Columns.Count - 1; i++)
            {
                ListViewDominios.Columns[i].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }

        }

        void DisplayCoins(Pais pais)
        {
            ListViewMoedas.Items.Clear();

            foreach (Currency moeda in pais.currencies)
            {
                ListViewItem item;

                item = ListViewMoedas.Items.Add(moeda.code);
                item.SubItems.Add(moeda.symbol);
                item.SubItems.Add(moeda.name);
            }
            for (int i = 0; i <= ListViewMoedas.Columns.Count - 1; i++)
            {
                ListViewMoedas.Columns[i].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
        }
    }
}
