﻿namespace Mundo
{
    partial class Estatistica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Estatistica));
            this.GridViewEstatistica = new System.Windows.Forms.DataGridView();
            this.ComboBoxContinentes = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelTotalPaises = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LabelMaisPop = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LabelMenosPop = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LabelMaiorDesigualdade = new System.Windows.Forms.Label();
            this.LabelMenosDesigual = new System.Windows.Forms.Label();
            this.LabelMaiorArea = new System.Windows.Forms.Label();
            this.LabelMenorArea = new System.Windows.Forms.Label();
            this.LabelTotalArea = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.LabelTotalPop = new System.Windows.Forms.Label();
            this.LabelMenorDesigualdade = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.GroupBoxPaises = new System.Windows.Forms.GroupBox();
            this.GroupBoxTotal = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewEstatistica)).BeginInit();
            this.GroupBoxPaises.SuspendLayout();
            this.GroupBoxTotal.SuspendLayout();
            this.SuspendLayout();
            // 
            // GridViewEstatistica
            // 
            this.GridViewEstatistica.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.GridViewEstatistica.AllowUserToAddRows = false;
            this.GridViewEstatistica.AllowUserToDeleteRows = false;
            this.GridViewEstatistica.AllowUserToResizeColumns = false;
            this.GridViewEstatistica.AllowUserToResizeRows = false;
            this.GridViewEstatistica.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GridViewEstatistica.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GridViewEstatistica.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewEstatistica.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.GridViewEstatistica.Location = new System.Drawing.Point(15, 61);
            this.GridViewEstatistica.Margin = new System.Windows.Forms.Padding(4);
            this.GridViewEstatistica.MultiSelect = false;
            this.GridViewEstatistica.Name = "GridViewEstatistica";
            this.GridViewEstatistica.ReadOnly = true;
            this.GridViewEstatistica.Size = new System.Drawing.Size(1310, 407);
            this.GridViewEstatistica.TabIndex = 0;
            // 
            // ComboBoxContinentes
            // 
            this.ComboBoxContinentes.FormattingEnabled = true;
            this.ComboBoxContinentes.Location = new System.Drawing.Point(192, 29);
            this.ComboBoxContinentes.Margin = new System.Windows.Forms.Padding(4);
            this.ComboBoxContinentes.Name = "ComboBoxContinentes";
            this.ComboBoxContinentes.Size = new System.Drawing.Size(272, 24);
            this.ComboBoxContinentes.TabIndex = 1;
            this.ComboBoxContinentes.SelectedIndexChanged += new System.EventHandler(this.ComboBoxContinentes_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Filtrar por continente:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Total de Países:";
            // 
            // LabelTotalPaises
            // 
            this.LabelTotalPaises.AutoSize = true;
            this.LabelTotalPaises.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTotalPaises.Location = new System.Drawing.Point(134, 37);
            this.LabelTotalPaises.Name = "LabelTotalPaises";
            this.LabelTotalPaises.Size = new System.Drawing.Size(11, 16);
            this.LabelTotalPaises.TabIndex = 4;
            this.LabelTotalPaises.Text = " ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Maior população:";
            // 
            // LabelMaisPop
            // 
            this.LabelMaisPop.AutoSize = true;
            this.LabelMaisPop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMaisPop.Location = new System.Drawing.Point(141, 37);
            this.LabelMaisPop.Name = "LabelMaisPop";
            this.LabelMaisPop.Size = new System.Drawing.Size(11, 16);
            this.LabelMaisPop.TabIndex = 6;
            this.LabelMaisPop.Text = " ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(133, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Menor população:";
            // 
            // LabelMenosPop
            // 
            this.LabelMenosPop.AutoSize = true;
            this.LabelMenosPop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMenosPop.Location = new System.Drawing.Point(145, 91);
            this.LabelMenosPop.Name = "LabelMenosPop";
            this.LabelMenosPop.Size = new System.Drawing.Size(11, 16);
            this.LabelMenosPop.TabIndex = 8;
            this.LabelMenosPop.Text = " ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(151, 16);
            this.label5.TabIndex = 9;
            this.label5.Text = "Maior desigualdade:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(382, 670);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(190, 16);
            this.label6.TabIndex = 10;
            this.label6.Text = "País menor desigualdade:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(561, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 16);
            this.label7.TabIndex = 11;
            this.label7.Text = "Maior área:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(561, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 16);
            this.label8.TabIndex = 12;
            this.label8.Text = "Menor área:";
            // 
            // LabelMaiorDesigualdade
            // 
            this.LabelMaiorDesigualdade.AutoSize = true;
            this.LabelMaiorDesigualdade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMaiorDesigualdade.Location = new System.Drawing.Point(163, 149);
            this.LabelMaiorDesigualdade.Name = "LabelMaiorDesigualdade";
            this.LabelMaiorDesigualdade.Size = new System.Drawing.Size(11, 16);
            this.LabelMaiorDesigualdade.TabIndex = 13;
            this.LabelMaiorDesigualdade.Text = " ";
            // 
            // LabelMenosDesigual
            // 
            this.LabelMenosDesigual.AutoSize = true;
            this.LabelMenosDesigual.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMenosDesigual.Location = new System.Drawing.Point(591, 670);
            this.LabelMenosDesigual.Name = "LabelMenosDesigual";
            this.LabelMenosDesigual.Size = new System.Drawing.Size(45, 16);
            this.LabelMenosDesigual.TabIndex = 14;
            this.LabelMenosDesigual.Text = "label3";
            // 
            // LabelMaiorArea
            // 
            this.LabelMaiorArea.AutoSize = true;
            this.LabelMaiorArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMaiorArea.Location = new System.Drawing.Point(654, 37);
            this.LabelMaiorArea.Name = "LabelMaiorArea";
            this.LabelMaiorArea.Size = new System.Drawing.Size(11, 16);
            this.LabelMaiorArea.TabIndex = 15;
            this.LabelMaiorArea.Text = " ";
            // 
            // LabelMenorArea
            // 
            this.LabelMenorArea.AutoSize = true;
            this.LabelMenorArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMenorArea.Location = new System.Drawing.Point(654, 91);
            this.LabelMenorArea.Name = "LabelMenorArea";
            this.LabelMenorArea.Size = new System.Drawing.Size(11, 16);
            this.LabelMenorArea.TabIndex = 16;
            this.LabelMenorArea.Text = " ";
            // 
            // LabelTotalArea
            // 
            this.LabelTotalArea.AutoSize = true;
            this.LabelTotalArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTotalArea.Location = new System.Drawing.Point(92, 149);
            this.LabelTotalArea.Name = "LabelTotalArea";
            this.LabelTotalArea.Size = new System.Drawing.Size(11, 16);
            this.LabelTotalArea.TabIndex = 18;
            this.LabelTotalArea.Text = " ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 149);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 16);
            this.label10.TabIndex = 17;
            this.label10.Text = "Área total:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 91);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 16);
            this.label11.TabIndex = 19;
            this.label11.Text = "População total:";
            // 
            // LabelTotalPop
            // 
            this.LabelTotalPop.AutoSize = true;
            this.LabelTotalPop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTotalPop.Location = new System.Drawing.Point(134, 91);
            this.LabelTotalPop.Name = "LabelTotalPop";
            this.LabelTotalPop.Size = new System.Drawing.Size(11, 16);
            this.LabelTotalPop.TabIndex = 20;
            this.LabelTotalPop.Text = " ";
            // 
            // LabelMenorDesigualdade
            // 
            this.LabelMenorDesigualdade.AutoSize = true;
            this.LabelMenorDesigualdade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMenorDesigualdade.Location = new System.Drawing.Point(722, 149);
            this.LabelMenorDesigualdade.Name = "LabelMenorDesigualdade";
            this.LabelMenorDesigualdade.Size = new System.Drawing.Size(11, 16);
            this.LabelMenorDesigualdade.TabIndex = 22;
            this.LabelMenorDesigualdade.Text = " ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(561, 149);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(155, 16);
            this.label12.TabIndex = 21;
            this.label12.Text = "Menor desigualdade:";
            // 
            // GroupBoxPaises
            // 
            this.GroupBoxPaises.Controls.Add(this.label3);
            this.GroupBoxPaises.Controls.Add(this.label7);
            this.GroupBoxPaises.Controls.Add(this.LabelMaisPop);
            this.GroupBoxPaises.Controls.Add(this.label8);
            this.GroupBoxPaises.Controls.Add(this.label4);
            this.GroupBoxPaises.Controls.Add(this.LabelMaiorArea);
            this.GroupBoxPaises.Controls.Add(this.LabelMenorDesigualdade);
            this.GroupBoxPaises.Controls.Add(this.LabelMenorArea);
            this.GroupBoxPaises.Controls.Add(this.LabelMenosPop);
            this.GroupBoxPaises.Controls.Add(this.LabelMaiorDesigualdade);
            this.GroupBoxPaises.Controls.Add(this.label12);
            this.GroupBoxPaises.Controls.Add(this.label5);
            this.GroupBoxPaises.Location = new System.Drawing.Point(336, 475);
            this.GroupBoxPaises.Name = "GroupBoxPaises";
            this.GroupBoxPaises.Size = new System.Drawing.Size(989, 190);
            this.GroupBoxPaises.TabIndex = 23;
            this.GroupBoxPaises.TabStop = false;
            this.GroupBoxPaises.Text = "Países";
            // 
            // GroupBoxTotal
            // 
            this.GroupBoxTotal.Controls.Add(this.label2);
            this.GroupBoxTotal.Controls.Add(this.LabelTotalPaises);
            this.GroupBoxTotal.Controls.Add(this.LabelTotalArea);
            this.GroupBoxTotal.Controls.Add(this.LabelTotalPop);
            this.GroupBoxTotal.Controls.Add(this.label10);
            this.GroupBoxTotal.Controls.Add(this.label11);
            this.GroupBoxTotal.Location = new System.Drawing.Point(15, 475);
            this.GroupBoxTotal.Name = "GroupBoxTotal";
            this.GroupBoxTotal.Size = new System.Drawing.Size(315, 190);
            this.GroupBoxTotal.TabIndex = 24;
            this.GroupBoxTotal.TabStop = false;
            this.GroupBoxTotal.Text = " ";
            // 
            // Estatistica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1368, 669);
            this.Controls.Add(this.GroupBoxTotal);
            this.Controls.Add(this.GroupBoxPaises);
            this.Controls.Add(this.LabelMenosDesigual);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboBoxContinentes);
            this.Controls.Add(this.GridViewEstatistica);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Estatistica";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estatistica";
            ((System.ComponentModel.ISupportInitialize)(this.GridViewEstatistica)).EndInit();
            this.GroupBoxPaises.ResumeLayout(false);
            this.GroupBoxPaises.PerformLayout();
            this.GroupBoxTotal.ResumeLayout(false);
            this.GroupBoxTotal.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView GridViewEstatistica;
        private System.Windows.Forms.ComboBox ComboBoxContinentes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LabelTotalPaises;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LabelMaisPop;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LabelMenosPop;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label LabelMaiorDesigualdade;
        private System.Windows.Forms.Label LabelMenosDesigual;
        private System.Windows.Forms.Label LabelMaiorArea;
        private System.Windows.Forms.Label LabelMenorArea;
        private System.Windows.Forms.Label LabelTotalArea;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label LabelTotalPop;
        private System.Windows.Forms.Label LabelMenorDesigualdade;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox GroupBoxPaises;
        private System.Windows.Forms.GroupBox GroupBoxTotal;
    }
}