﻿namespace Mundo
{
    partial class MapaMundo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MapaMundo));
            this.label1 = new System.Windows.Forms.Label();
            this.ComboBoxPaises = new System.Windows.Forms.ComboBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.LabelInfo = new System.Windows.Forms.Label();
            this.ListViewMoedas = new System.Windows.Forms.ListView();
            this.ListViewDominios = new System.Windows.Forms.ListView();
            this.ControlMapa = new GMap.NET.WindowsForms.GMapControl();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Países:";
            // 
            // ComboBoxPaises
            // 
            this.ComboBoxPaises.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxPaises.FormattingEnabled = true;
            this.ComboBoxPaises.Location = new System.Drawing.Point(93, 12);
            this.ComboBoxPaises.Name = "ComboBoxPaises";
            this.ComboBoxPaises.Size = new System.Drawing.Size(376, 21);
            this.ComboBoxPaises.TabIndex = 3;
            this.ComboBoxPaises.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPaises_SelectedIndexChanged);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(500, 11);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(182, 85);
            this.richTextBox1.TabIndex = 5;
            this.richTextBox1.Text = "";
            // 
            // LabelInfo
            // 
            this.LabelInfo.AutoSize = true;
            this.LabelInfo.Location = new System.Drawing.Point(27, 68);
            this.LabelInfo.Name = "LabelInfo";
            this.LabelInfo.Size = new System.Drawing.Size(35, 13);
            this.LabelInfo.TabIndex = 6;
            this.LabelInfo.Text = "label2";
            // 
            // ListViewMoedas
            // 
            this.ListViewMoedas.GridLines = true;
            this.ListViewMoedas.Location = new System.Drawing.Point(699, 12);
            this.ListViewMoedas.Name = "ListViewMoedas";
            this.ListViewMoedas.Size = new System.Drawing.Size(284, 84);
            this.ListViewMoedas.TabIndex = 7;
            this.ListViewMoedas.UseCompatibleStateImageBehavior = false;
            this.ListViewMoedas.View = System.Windows.Forms.View.Details;
            // 
            // ListViewDominios
            // 
            this.ListViewDominios.GridLines = true;
            this.ListViewDominios.Location = new System.Drawing.Point(999, 12);
            this.ListViewDominios.Name = "ListViewDominios";
            this.ListViewDominios.Size = new System.Drawing.Size(66, 84);
            this.ListViewDominios.TabIndex = 8;
            this.ListViewDominios.UseCompatibleStateImageBehavior = false;
            this.ListViewDominios.View = System.Windows.Forms.View.Details;
            // 
            // ControlMapa
            // 
            this.ControlMapa.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ControlMapa.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ControlMapa.Bearing = 0F;
            this.ControlMapa.CanDragMap = true;
            this.ControlMapa.CausesValidation = false;
            this.ControlMapa.EmptyTileColor = System.Drawing.Color.Navy;
            this.ControlMapa.GrayScaleMode = false;
            this.ControlMapa.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.ControlMapa.LevelsKeepInMemmory = 5;
            this.ControlMapa.Location = new System.Drawing.Point(30, 102);
            this.ControlMapa.MarkersEnabled = true;
            this.ControlMapa.MaximumSize = new System.Drawing.Size(1035, 756);
            this.ControlMapa.MaxZoom = 5;
            this.ControlMapa.MinimumSize = new System.Drawing.Size(1035, 756);
            this.ControlMapa.MinZoom = 2;
            this.ControlMapa.MouseWheelZoomEnabled = true;
            this.ControlMapa.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.ControlMapa.Name = "ControlMapa";
            this.ControlMapa.NegativeMode = false;
            this.ControlMapa.PolygonsEnabled = true;
            this.ControlMapa.RetryLoadTile = 0;
            this.ControlMapa.RoutesEnabled = true;
            this.ControlMapa.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.ControlMapa.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.ControlMapa.ShowTileGridLines = false;
            this.ControlMapa.Size = new System.Drawing.Size(1035, 756);
            this.ControlMapa.TabIndex = 9;
            this.ControlMapa.Zoom = 2D;
            // 
            // MapaMundo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1077, 890);
            this.Controls.Add(this.ControlMapa);
            this.Controls.Add(this.ListViewDominios);
            this.Controls.Add(this.ListViewMoedas);
            this.Controls.Add(this.LabelInfo);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboBoxPaises);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MapaMundo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mapa Mundo";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ComboBoxPaises;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label LabelInfo;
        private System.Windows.Forms.ListView ListViewMoedas;
        private System.Windows.Forms.ListView ListViewDominios;
        private GMap.NET.WindowsForms.GMapControl ControlMapa;
    }
}