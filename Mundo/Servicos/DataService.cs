﻿
namespace Mundo.Servicos
{
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Data.SQLite;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows.Forms;
    using System.Globalization;

    public class DataService
    {
        #region Atributos

        // Definir variavel connection do tipo SQLiteConnection
        private SQLiteConnection connection;

        // Definir variavel command do tipo SQLiteConmmand
        private SQLiteCommand command;

        // Definir  variavel  dialogService
        private DialogService dialogService;
        #endregion

        #region Construtor

        //..................................................................................................................................................
        //.
        //.                                               Criação das Tabelas
        //.
        //..................................................................................................................................................


        public DataService()
        {
            // Instanciar dialogService para se poder aceder
            dialogService = new DialogService();

            // se a diretoria Data não existe então criar-la 
            if (!Directory.Exists(@"Data"))
            {
                Directory.CreateDirectory(@"Data");
            }


            // Caminho indicando a pasta onde vai ser criado o ficheiro de bases de dados Paises.sqlite
            var path = @"Data\Paises.sqlite";

            //Definir e inicializar Limpando a variavel sqlcommand do tipo String
            string sqlcommand = null;


            try
            {
                //Instanciar a ligação 
                connection = new SQLiteConnection("Data Source=" + path);

                //Abrir a ligação 
                connection.Open();

                // atribuir a string com as instruções de sql
                sqlcommand = String.Format("create table if not exists Paises(IdPais integer not null primary key autoincrement, name varchar(150), Capital varchar(150), region varchar(100),area float, population int, gini float,  flag varchar(200))");

                //Instanciar command utilizando a string de sql e a ligação
                command = new SQLiteCommand(sqlcommand, connection);

                // Execução do query
                command.ExecuteNonQuery();

                sqlcommand = String.Format("create table if not exists Moedas (IdMoeda integer not null primary key autoincrement, code varchar (5), name varchar (250), symbol varchar (1), Pais integer, foreign key (Pais) references Paises (idPais))");

                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();

                sqlcommand = String.Format("create table if not exists Dominios (IdDominio integer not null primary key autoincrement, dominio varchar (5),  Pais  integer, foreign key (Pais) references Paises (idPais))");

                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();

                sqlcommand = String.Format("create table if not exists Traduz (IdTraduz integer not null primary key autoincrement, br varchar (5), de varchar (5), es varchar (5), fa varchar (5), fr varchar (5), hr varchar (5), it varchar (5), ja varchar (5), nl varchar (5), pt varchar (250), Pais integer,foreign key (Pais) references Paises (idPais))");

                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();

                sqlcommand = String.Format("create table if not exists Mapas (IdMapa integer not null primary key autoincrement, latlng varchar (25),  Pais  integer, foreign key (Pais) references Paises (idPais))");

                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //erro na criação das Tabelas de SQL
                dialogService.DisplayMessage("Erro na criação das Tabelas de SQLite - DataService ", e.Message);
            }


        }
        #endregion

        #region Metodos
        //..................................................................................................................................................
        //.
        //.                                               Obter Dados das Tabelas
        //.
        //..................................................................................................................................................

        //Obter Dados da Tabela Pais
        public List<Pais> GetDataPaises()
        {
            //insatanciar  lista paises
            List<Pais> paises = new List<Pais>();

            try
            {
                // altera CurrentCulture para en-US 
                //para não haver erros de conversão entre o separador ponto (. ->"en-US") tal como vem da api
                //e o separador virgula (, ->"pt-PT" ) definido nos nossos pc´s
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                // atribuir a string com as instruções de sql
                string sql = "select IdPais, name, capital, region, population, area, gini, flag from Paises";

                //Instanciar command do tipo  SQLiteCommand utilizando a string de sql e a ligação
                command = new SQLiteCommand(sql, connection);

                //Instanciar reader do tipo  SQLiteDataReader
                SQLiteDataReader reader = command.ExecuteReader();

                // utilizando o metodo Read do DataReader
                while (reader.Read())
                {
                    // adicionar à lista de paises adicionando objectos do tipo Pais
                    paises.Add(new Pais
                    {
                        // fazendo a conversão para Int32 pois o IdPais não aceita Cast
                        IdPais = Convert.ToInt32(reader["IdPais"]),

                        //fazendo Cast
                        name = (string)reader["name"],
                        capital = (string)reader["capital"],
                        region = (string)reader["region"],

                        // fazendo a conversão para Double 
                        area = Convert.ToDouble(reader["area"]),

                        //fazendo Cast
                        population = (int)reader["population"],

                        gini = Convert.ToDouble(reader["gini"]),
                        flag = (string)reader["flag"],
                        currencies = GetDataMoedas(Convert.ToInt32(reader["IdPais"])),
                        translations = GetDataTraduz(Convert.ToInt32(reader["IdPais"])),
                        topLevelDomain = GetDataDominios(Convert.ToInt32(reader["IdPais"])),
                        latlng = GetDataLatLong(Convert.ToInt32(reader["IdPais"]))

                    });
                }

                connection.Close();
                // altera CurrentCulture para pt-PT.
                //para não haver erros de conversão entre o separador ponto (. ->"en-US") tal como vem da api
                //e o separador virgula (, ->"pt-PT" ) definido nos nossos pc´s
                Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-PT");

                // devolve a lista paises
                return paises;

            }
            catch (Exception e)
            {
                //Erro ao obter dados da tabela Pais
                dialogService.DisplayMessage("Erro no GetDataPaises - DataService", e.Message);

                //retorna vazio
                return null;
            }
        }

        //Obter Dados da Tabela Traduz...........................................................................................................................
        public Translations GetDataTraduz(int Idpais)
        {
            Translations translations = new Translations();

            try
            {
                string sql = "select  br, de, es, fa , fr, hr, it, ja, nl, pt from traduz where Pais=" + Idpais + ";";
                command = new SQLiteCommand(sql, connection);

                SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {

                    translations.br = (string)reader["br"];
                    translations.de = (string)reader["de"];
                    translations.es = (string)reader["es"];
                    translations.fa = (string)reader["fa"];
                    translations.fr = (string)reader["fr"];
                    translations.hr = (string)reader["hr"];
                    translations.it = (string)reader["it"];
                    translations.ja = (string)reader["ja"];
                    translations.nl = (string)reader["nl"];
                    translations.pt = (string)reader["pt"];



                }

                return translations;




            }
            catch (Exception e)
            {

                dialogService.DisplayMessage("Erro", e.Message);
                return null;
            }
        }


        //Obter Dados da lista latlng.....................................................................................................................
        public List<object> GetDataLatLong(int Idpais)
        {
            List<object> latlng = new List<object>();
            string sql = "select latlng, Pais from Mapas where Pais=" + Idpais + ";";
            command = new SQLiteCommand(sql, connection);

            SQLiteDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                latlng.Add(reader["latlng"].ToString());
            }

            return latlng;
        }


        //Obter Dados da Tabela Moedas...................................................................................................................
        public List<Currency> GetDataMoedas(int Idpais)
        {

            List<Currency> moedas = new List<Currency>();
            try
            {

                string sql = "select  name, code, symbol, Pais from Moedas where Pais=" + Idpais + ";";
                command = new SQLiteCommand(sql, connection);

                SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    moedas.Add(new Currency
                    {

                        code = (string)reader["code"],
                        name = (string)reader["name"],
                        symbol = (string)reader["symbol"],

                    });
                }

                return moedas;
            }
            catch (Exception e)
            {

                dialogService.DisplayMessage("Erro", e.Message);
                return null;
            }
        }

        //Obter Dados da Tabela Dominios...............................................................................................................
        public List<string> GetDataDominios(int Idpais)
        {
            List<string> dominios = new List<string>();
            try
            {

                string sql = "select dominio, Pais from Dominios where Pais=" + Idpais + ";";
                command = new SQLiteCommand(sql, connection);

                SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    dominios.Add(reader["dominio"].ToString());
                }

                return dominios;




            }
            catch (Exception e)
            {

                dialogService.DisplayMessage("Erro", e.Message);
                return null;
            }
        }

        //..................................................................................................................................................
        //.
        //.                                             Gravar  Dados nas Tabelas
        //.
        //..................................................................................................................................................



        public void SaveData(List<Pais> Paises, ProgressBar ProgressData, Label LabelStatusData)
        {
            ProgressData.Maximum = Paises.Count();


            int pais_id = 0;
            //Garavr Dados da Tabela Paises...............................................................................................................
            try
            {

                foreach (var pais in Paises)
                {
                    string path = "Imagens\\" + pais.name + ".svg";

                    string sql = "insert into Paises( name, capital, region, area, population, gini, flag) values(@name, @capital, @region,@area, @population, @gini, @flag ); SELECT last_insert_rowid(); ";


                    command = new SQLiteCommand(sql, connection);


                    command.Parameters.AddWithValue("@name", pais.name);
                    command.Parameters.AddWithValue("@capital", pais.capital);
                    command.Parameters.AddWithValue("@region", pais.region);
                    command.Parameters.AddWithValue("@population", pais.population);
                    if (pais.area != null)
                    {

                        command.Parameters.AddWithValue("@area", pais.area);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@area", 0);
                    }
                    if (pais.gini != null)
                    {
                        command.Parameters.AddWithValue("@gini", pais.gini);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@gini", 0);
                    }




                    command.Parameters.AddWithValue("@flag", path);



                    pais_id = Convert.ToInt32(command.ExecuteScalar());
                    ProgressData.Value = pais_id;
                    LabelStatusData.Text = ("A carregar pais " + pais_id + " - " + pais.name);
                    LabelStatusData.Update();

                    //Gravar dados na Tabela Moedas..............................................................................................................
                    foreach (var moeda in pais.currencies)
                    {
                        sql = string.Format("insert into Moedas (Pais,code, name, symbol) values (@Pais,@code, @name, @symbol)");

                        command = new SQLiteCommand(sql, connection);

                        command.Parameters.AddWithValue("@Pais", pais_id);
                        if (moeda.code != null)
                        {
                            command.Parameters.AddWithValue("@code", moeda.code);
                        }
                        else
                        {

                            command.Parameters.AddWithValue("@code", "N/A");
                        }
                        if (moeda.name != null)
                        {
                            command.Parameters.AddWithValue("@name", moeda.name);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@name", "N/A");
                        }

                        if (moeda.symbol != null)
                        {
                            command.Parameters.AddWithValue("@symbol", moeda.symbol);
                        }
                        else
                        {

                            command.Parameters.AddWithValue("@Symbol", "N/A");
                        }

                        command.ExecuteNonQuery();

                    }
                    //Gravar dados na  Tabela Dominios...............................................................................................................
                    foreach (var dominio in pais.topLevelDomain)
                    {
                        sql = string.Format("insert into Dominios (dominio,Pais) values (@dominio,@Pais)");

                        command = new SQLiteCommand(sql, connection);
                        command.Parameters.AddWithValue("@dominio", dominio);
                        command.Parameters.AddWithValue("@Pais", pais_id);

                        command.ExecuteNonQuery();

                    }

                    //Gravar dados na  Tabela Traduz...............................................................................................................
                    sql = string.Format("insert into Traduz (br, de, es, fr, ja, it, nl, hr, fa, pt, Pais) values (@br, @de, @es, @fr, @ja, @it, @nl, @hr, @fa, @pt,@Pais)");

                    command = new SQLiteCommand(sql, connection);

                    if (pais.translations.br != null)
                    {
                        command.Parameters.AddWithValue("@br", pais.translations.br);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@br", "N/A");
                    }

                    if (pais.translations.de != null)
                    {
                        command.Parameters.AddWithValue("@de", pais.translations.de);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@de", "N/A");
                    }

                    if (pais.translations.es != null)
                    {
                        command.Parameters.AddWithValue("@es", pais.translations.es);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@es", "N/A");
                    }

                    if (pais.translations.fr != null)
                    {
                        command.Parameters.AddWithValue("@fr", pais.translations.fr);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@fr", "N/A");
                    }



                    if (pais.translations.ja != null)
                    {
                        command.Parameters.AddWithValue("@ja", pais.translations.ja);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@ja", "N/A");
                    }

                    if (pais.translations.it != null)
                    {
                        command.Parameters.AddWithValue("@it", pais.translations.it);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@it", "N/A");
                    }


                    if (pais.translations.nl != null)
                    {
                        command.Parameters.AddWithValue("@nl", pais.translations.nl);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@nl", "N/A");
                    }


                    if (pais.translations.hr != null)
                    {
                        command.Parameters.AddWithValue("@hr", pais.translations.hr);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@hr", "N/A");
                    }


                    if (pais.translations.fa != null)
                    {
                        command.Parameters.AddWithValue("@fa", pais.translations.fa);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@fa", "N/A");
                    }


                    if (pais.translations.pt != null)
                    {
                        command.Parameters.AddWithValue("@pt", pais.translations.pt);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@pt", "N/A");
                    }

                    command.Parameters.AddWithValue("@Pais", pais_id);

                    command.ExecuteNonQuery();
                    // //Gravar dados na lista latlng...............................................................................................................

                    foreach (var latlng in pais.latlng)
                    {
                        sql = string.Format("insert into Mapas (latlng,Pais) values (@latlng,@Pais)");

                        command = new SQLiteCommand(sql, connection);
                        command.Parameters.AddWithValue("@latlng", latlng);
                        command.Parameters.AddWithValue("@Pais", pais_id);

                        command.ExecuteNonQuery();

                    }


                }
            }
            catch (Exception e)
            {

                dialogService.DisplayMessage("Erro no SaveData em Dataservice", e.Message);
            }
        }
        //..................................................................................................................................................
        //.
        //.                                               Apagar dados das Tabelas
        //.
        //..................................................................................................................................................

        internal void DeleteData()
        {

            try
            {
                string sql = "delete from Paises;delete from Moedas;delete from Dominios;delete from Traduz; delete from Mapas; UPDATE SQLITE_SEQUENCE SET SEQ = 0 WHERE NAME = 'Paises'; " +
                    "UPDATE SQLITE_SEQUENCE SET SEQ = 0 WHERE NAME = 'Moedas';UPDATE SQLITE_SEQUENCE SET SEQ = 0 WHERE NAME = 'Dominios';UPDATE SQLITE_SEQUENCE SET SEQ = 0 WHERE NAME = 'Traduz'; UPDATE SQLITE_SEQUENCE SET SEQ = 0 WHERE NAME = 'Mapas'; ";



                command = new SQLiteCommand(sql, connection);
                command.ExecuteNonQuery();

            }
            catch (Exception e)
            {

                dialogService.DisplayMessage("Erro no DeleteData em DataService ", e.Message);
            }
        }
        #endregion



    }
}
