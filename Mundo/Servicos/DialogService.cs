﻿namespace Mundo.Servicos
{
    using System.Windows.Forms;

    public class DialogService

    {
        public void DisplayMessage(string message,string title)
        {
            MessageBox.Show(title, message, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

    }
}
