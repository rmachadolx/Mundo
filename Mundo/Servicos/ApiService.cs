﻿namespace Mundo.Servicos
{
    using Modelos;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Drawing.Imaging;
    using System.Globalization;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public class ApiService
    {
        //Definir e Instanciar dialogService para se poder aceder
        private DialogService dialogService = new DialogService();



        public async Task<Response> GetPaises(string urlBase, string controller, ProgressBar ProgressBar1, Label LabelStatus)
        {
            //se a diretoria Imagens existir
            if (Directory.Exists(@"Imagens"))
            {
                // Apagar o conteudo da diretoria Imagens
                DirectoryInfo di = new DirectoryInfo(@"Imagens");
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }

                // Apagar a diretoria Imagens
                Directory.Delete(@"Imagens");
            }

            // Criar a diretoria Imagens
            Directory.CreateDirectory(@"Imagens");

            try
            {
                // criar um httpClient
                var Client = new HttpClient();

                // definir endereço de base com base no endereço "https://restcountries.eu" vindo do Form1
                Client.BaseAddress = new Uri(urlBase);

                // definir variavel response vai chamar o metodo assincrono com o controlador   "rest/v2/all" na api
                var response = await Client.GetAsync(controller);

                // variavel result que recee o resultado da leitura assincrina vinda da api
                var result = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)

                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = result
                    };
                }
             
                // colocar o resultado na lista Paises
                var paises = JsonConvert.DeserializeObject<List<Pais>>(result);



                // cnta paises
                int Counter = 0;

                // incializar ProgressBar1 a 0
                ProgressBar1.Value = 0;

                //definir maximo para ProgressBar1
                ProgressBar1.Maximum = paises.Count;

                // indicar na label status
                LabelStatus.Text = "A carregar as bandeiras...";
                foreach (Pais pais in paises)
                {
                    try
                    {
                        //carregar os ficheiros svg das bandeiras na pasta Imagens

                        string path = @"Imagens\" + pais.name + ".svg";
                     
                        //Utilizando  o objecto webClient , uma instancia da classe WebClient
                            using (WebClient webClient = new WebClient())
                            {

                                {
                                   // utilizando o metodo DownloadFile ando o nome e o camino
                                    webClient.DownloadFile(pais.flag, path);
                                }
                                // atualizar ProgressBar1 com ovalor do contador
                                ProgressBar1.Value = Counter;

                                // informação da bendeira numero e pais que está a ser carregada 
                                LabelStatus.Text = String.Format("A carregar bandeira {0} - {1}", Counter, pais.name);
                                // atualizar LabelStatus se não, não muda 
                                LabelStatus.Update();
                            }
                       // incrementar contador
                        Counter++;



                    }
                    catch (Exception ex)
                    {
                       //Erro na imagem da api
                        dialogService.DisplayMessage("Erro na imagem vinda da Api - ApiService", ex.Message + " " + pais.translations.pt);

                    }


                }


                return new Response
                {
                    //Correu bem o Carregamento dos paises vindos da api. 
                    //Utilizando a dialogService para:
                    // atualizar IsSucessa true  e Message
                    IsSucess = true,
                    Message = ("Foram atualizados os paises."),

                    // retorna paises
                    Result = paises,


                };

            }
            catch (Exception ex)
            {
                return new Response
                {
                    //Erro no Carregamento dos paises vindos da api .
                    //utilizando a dialogService para:
                    // atualizar IsSucessa false  e Message
                    IsSucess = false,
                    Message = ex.Message
                    // não retorna  nada para result
                };

            }


        }


    }
}
