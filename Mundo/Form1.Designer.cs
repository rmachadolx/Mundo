﻿namespace Mundo
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ComboBoxPaises = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TreeViewPaises = new System.Windows.Forms.TreeView();
            this.LabelStatus = new System.Windows.Forms.Label();
            this.LabelResultado = new System.Windows.Forms.Label();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.ProgressBar1 = new System.Windows.Forms.ProgressBar();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.irParaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.porContinenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verNoMapaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estatísticaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.ComboBoxLinguas = new System.Windows.Forms.ComboBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ComboBoxPaises
            // 
            this.ComboBoxPaises.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxPaises.FormattingEnabled = true;
            this.ComboBoxPaises.Location = new System.Drawing.Point(90, 65);
            this.ComboBoxPaises.Name = "ComboBoxPaises";
            this.ComboBoxPaises.Size = new System.Drawing.Size(376, 21);
            this.ComboBoxPaises.TabIndex = 0;
            this.ComboBoxPaises.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPaises_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Países:";
            // 
            // TreeViewPaises
            // 
            this.TreeViewPaises.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TreeViewPaises.Location = new System.Drawing.Point(479, 99);
            this.TreeViewPaises.Name = "TreeViewPaises";
            this.TreeViewPaises.Size = new System.Drawing.Size(464, 313);
            this.TreeViewPaises.TabIndex = 2;
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Location = new System.Drawing.Point(487, 460);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(10, 13);
            this.LabelStatus.TabIndex = 3;
            this.LabelStatus.Text = " ";
            // 
            // LabelResultado
            // 
            this.LabelResultado.AutoSize = true;
            this.LabelResultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelResultado.Location = new System.Drawing.Point(24, 476);
            this.LabelResultado.Name = "LabelResultado";
            this.LabelResultado.Size = new System.Drawing.Size(12, 16);
            this.LabelResultado.TabIndex = 4;
            this.LabelResultado.Text = " ";
            // 
            // PictureBox1
            // 
            this.PictureBox1.Location = new System.Drawing.Point(27, 99);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(439, 313);
            this.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox1.TabIndex = 5;
            this.PictureBox1.TabStop = false;
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Location = new System.Drawing.Point(479, 476);
            this.ProgressBar1.MarqueeAnimationSpeed = 5;
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(464, 23);
            this.ProgressBar1.Step = 1;
            this.ProgressBar1.TabIndex = 6;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.irParaToolStripMenuItem,
            this.sobreToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(979, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // irParaToolStripMenuItem
            // 
            this.irParaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.porContinenteToolStripMenuItem,
            this.verNoMapaToolStripMenuItem,
            this.estatísticaToolStripMenuItem});
            this.irParaToolStripMenuItem.Name = "irParaToolStripMenuItem";
            this.irParaToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.irParaToolStripMenuItem.Text = "Ir para";
            // 
            // porContinenteToolStripMenuItem
            // 
            this.porContinenteToolStripMenuItem.Name = "porContinenteToolStripMenuItem";
            this.porContinenteToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.porContinenteToolStripMenuItem.Text = "Por Continente";
            this.porContinenteToolStripMenuItem.Click += new System.EventHandler(this.porContinenteToolStripMenuItem_Click);
            // 
            // verNoMapaToolStripMenuItem
            // 
            this.verNoMapaToolStripMenuItem.Name = "verNoMapaToolStripMenuItem";
            this.verNoMapaToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.verNoMapaToolStripMenuItem.Text = "Ver no mapa";
            this.verNoMapaToolStripMenuItem.Click += new System.EventHandler(this.verNoMapaToolStripMenuItem_Click);
            // 
            // estatísticaToolStripMenuItem
            // 
            this.estatísticaToolStripMenuItem.Name = "estatísticaToolStripMenuItem";
            this.estatísticaToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.estatísticaToolStripMenuItem.Text = "Estatística";
            this.estatísticaToolStripMenuItem.Click += new System.EventHandler(this.estatísticaToolStripMenuItem_Click);
            // 
            // sobreToolStripMenuItem
            // 
            this.sobreToolStripMenuItem.Name = "sobreToolStripMenuItem";
            this.sobreToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.sobreToolStripMenuItem.Text = "Sobre...";
            this.sobreToolStripMenuItem.Click += new System.EventHandler(this.sobreToolStripMenuItem_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(476, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "Nome do país em:";
            // 
            // ComboBoxLinguas
            // 
            this.ComboBoxLinguas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxLinguas.FormattingEnabled = true;
            this.ComboBoxLinguas.Location = new System.Drawing.Point(606, 66);
            this.ComboBoxLinguas.Name = "ComboBoxLinguas";
            this.ComboBoxLinguas.Size = new System.Drawing.Size(337, 21);
            this.ComboBoxLinguas.TabIndex = 9;
            this.ComboBoxLinguas.SelectedIndexChanged += new System.EventHandler(this.ComboBoxLinguas_SelectedIndexChanged);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(703, 419);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(240, 13);
            this.linkLabel1.TabIndex = 13;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "https://pt.wikipedia.org/wiki/Coeficiente_de_Gini";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(476, 419);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(221, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "*Gini - Índice de desigualdade Social;";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 618);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.ComboBoxLinguas);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ProgressBar1);
            this.Controls.Add(this.PictureBox1);
            this.Controls.Add(this.LabelResultado);
            this.Controls.Add(this.LabelStatus);
            this.Controls.Add(this.TreeViewPaises);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboBoxPaises);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Países do Mundo";
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboBoxPaises;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TreeView TreeViewPaises;
        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.Label LabelResultado;
        private System.Windows.Forms.PictureBox PictureBox1;
        private System.Windows.Forms.ProgressBar ProgressBar1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ComboBoxLinguas;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem irParaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem porContinenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verNoMapaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estatísticaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem;
    }
}

