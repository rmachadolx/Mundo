﻿namespace Mundo
{
    using System.Collections.Generic;
    using Modelos;
    public class Pais

    {
        public int IdPais { get; set; }

        public string name { get; set; }
        
        public string capital { get; set; }

        public string region { get; set; }
        public double? area { get; set; }

        public decimal population { get; set; }
        public double? gini { get; set; }
        public string flag { get; set; }

        public List<Currency> currencies { get; set; }

        public List<string> topLevelDomain { get; set; }
        public List<object> latlng { get; set; }
        public Translations translations { get; set; }



    }
}
