﻿

namespace Mundo
{
    using Mundo.Servicos;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using System.Windows.Forms;

    using System.ComponentModel;
    using System.Data;

    using System.Text;
  
    using Microsoft.VisualBasic;
    using System.Collections;

    using System.Diagnostics;
    

    public partial class Estatistica : Form
    {
        private List<Pais> Paises;
        private DataService dataService;

        public Estatistica()
        {
            InitializeComponent();
            Paises = new List<Pais>();
            dataService = new DataService();
            Paises = dataService.GetDataPaises();
            var listaContinentes = Paises.Select(x => x.region).Distinct().ToList();

            ComboBoxContinentes.Items.Add("Mundo");
            foreach (var item in listaContinentes)
            {
                if (item != String.Empty)
                {
                    ComboBoxContinentes.Items.Add(item);
                }

            }

            GridViewEstatistica.Columns.Add("colId", "Nº");
            GridViewEstatistica.Columns.Add("colNome", "Nome");
            GridViewEstatistica.Columns.Add("colCapital", "Capital");
            GridViewEstatistica.Columns.Add("colRegiao", "Continente");
            GridViewEstatistica.Columns.Add("colPopulacao", "População");
            GridViewEstatistica.Columns.Add("colArea", "Área");
            GridViewEstatistica.Columns.Add("colDensidade", "Densidade Populacional");
            GridViewEstatistica.Columns.Add("colGini", "Indice Desigualdade");
            TodosPaises();
        }



        private void TodosPaises()
        {

            ComboBoxContinentes.SelectedIndex = 0;





            var paises = from pais in Paises
                         select pais;
            Calcular(paises);


        }

        private void ComboBoxContinentes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBoxContinentes.SelectedIndex == 0)
            {
                TodosPaises();
              
            }
            else
            {



                var paises = from pais in Paises
                             where pais.region == ComboBoxContinentes.SelectedItem.ToString()
                             select pais;

                Calcular(paises);
            }
            GroupBoxTotal.Text = ComboBoxContinentes.SelectedItem.ToString();
        }  

        private void Calcular(IEnumerable<Pais> paises)
        {
            decimal totalPop = 0;
            decimal totalArea = 0;
            string maisPop;
            string menosPop;
            string maisArea;
            string menosArea;
            string maisGini;
            string menosGini;
           totalPop= Convert.ToDecimal(paises.Sum(x => x.population));
            totalArea = Convert.ToDecimal(paises.Sum(x => x.area));
            LabelTotalArea.Text = String.Format("{0:N3} km quadrados", totalArea);
            LabelTotalPaises.Text = paises.Count().ToString();
            LabelTotalPop.Text = String.Format("{0:N0} habitantes",totalPop);


         decimal    maxValue = paises.Max(x => x.population);



            var resultMax = from p in paises
                            where maxValue == p.population
                            select p.name;

            LabelMaisPop.BackColor = Color.Red;
            LabelMaisPop.Text = String.Format("{0} com {1:N0} habtantes", resultMax.First(), maxValue);
            maisPop = resultMax.First();

            var resultPopNoZero = from p in paises
                                  where p.population != 0
                                  select p;

            decimal minValue = resultPopNoZero.Min(x => x.population);

            var resultMin = from p in paises
                            where minValue == p.population
                            select p.name;



            LabelMenosPop.BackColor = Color.Pink;
            LabelMenosPop.Text = String.Format("{0} com {1:N0} habtantes", resultMin.First(), minValue);
            menosPop = resultMin.First();


            double? maxValueGini = paises.Max(x => x.gini);

            var resultMaxGini = from p in paises
                                where maxValueGini == p.gini
                                select p.name;
            bool flag = false;
            foreach (var item in resultMaxGini)
            {
                if (resultMaxGini.First() == "Antarctica")
                {
                    flag = true;
                }
            }

            LabelMaiorDesigualdade.BackColor = Color.Yellow;
            LabelMaiorDesigualdade.Text = String.Format("{0} com gini = {1}", resultMaxGini.First(), maxValueGini);
            maisGini = resultMaxGini.First();

            var resultGiniNoZeros = from p in paises
                                    where p.gini != 0
                                    select p;


            double? minValueGini = resultGiniNoZeros.Min(x => x.gini);


            var resultMinGini = from p in paises
                                where minValueGini == p.gini
                                select p.name;

            LabelMenorDesigualdade.BackColor = Color.Green;
            if (!flag)
            {
                LabelMenorDesigualdade.Text = String.Format("{0} com gini = {1} .", resultMinGini.First(), minValueGini);
                menosGini = resultMinGini.First();
            }
            else
            {
                LabelMenorDesigualdade.Text = String.Format("Antarctica com gini = 0");
                menosGini = "Antarctica";
            }






            double? maxValueArea = paises.Max(x => x.area);


            var resultMaxArea = from p in paises
                                where maxValueArea == p.area
                                select p.name;

            LabelMaiorArea.BackColor = Color.Brown;
            LabelMaiorArea.Text = String.Format("{0} com {1:N2} km quadrados", resultMaxArea.First(), maxValueArea);
            maisArea = resultMaxArea.First();

            var resultAreaNoZeros = from p in paises
                                    where p.area != 0
                                    select p;

            double? minValueArea = resultAreaNoZeros.Min(x => x.area);


            var resultMinArea = from p in resultAreaNoZeros
                                where minValueArea == p.area
                                select p.name;

            LabelMenorArea.BackColor = Color.SandyBrown;
            LabelMenorArea.Text = String.Format("{0} com {1:N2} km quadrados.", resultMinArea.First(), minValueArea.ToString());
            menosArea = resultMinArea.First();

            GridViewEstatistica.Rows.Clear();
            GridViewEstatistica.Refresh();
            int linha = 0;
            foreach (Pais pais in paises)
            {

                DataGridViewRow registo = new DataGridViewRow();

                GridViewEstatistica.Rows.Add(registo);

                GridViewEstatistica.Rows[linha].Cells[0].Value = pais.IdPais;
                GridViewEstatistica.Rows[linha].Cells[1].Value = pais.name;
                GridViewEstatistica.Rows[linha].Cells[2].Value = pais.capital;
                GridViewEstatistica.Rows[linha].Cells[3].Value = pais.region;
                GridViewEstatistica.Rows[linha].Cells[4].Value = String.Format("{0:N0}", pais.population);
                GridViewEstatistica.Rows[linha].Cells[5].Value = String.Format("{0:N2}", pais.area);
                double densidade = (Math.Round(Convert.ToDouble(Convert.ToDouble(pais.population) / pais.area), 2));

                GridViewEstatistica.Rows[linha].Cells[6].Value = densidade;
                GridViewEstatistica.Rows[linha].Cells[7].Value = String.Format("{0:N1}", pais.gini);

                DataGridViewCellStyle vermelho = new DataGridViewCellStyle();
                vermelho.BackColor = Color.Red;

                DataGridViewCellStyle rosa = new DataGridViewCellStyle();
                rosa.BackColor = Color.Pink;

                DataGridViewCellStyle castanho = new DataGridViewCellStyle();
                castanho.BackColor = Color.Brown;

                DataGridViewCellStyle castanhoAreia = new DataGridViewCellStyle();
                castanhoAreia.BackColor = Color.SandyBrown;

                DataGridViewCellStyle amarelo = new DataGridViewCellStyle();
                amarelo.BackColor = Color.Yellow;

                DataGridViewCellStyle verde = new DataGridViewCellStyle();
                verde.BackColor = Color.Green;

                if ((string)(GridViewEstatistica.Rows[linha].Cells[1].Value) == menosPop)
                {
                    GridViewEstatistica.Rows[linha].DefaultCellStyle = rosa;
                }

                if ((string)GridViewEstatistica.Rows[linha].Cells[1].Value == maisPop)
                {
                    GridViewEstatistica.Rows[linha].DefaultCellStyle = vermelho;
                }

                if ((string)(GridViewEstatistica.Rows[linha].Cells[1].Value) == menosArea)
                {
                    GridViewEstatistica.Rows[linha].DefaultCellStyle = castanhoAreia;
                }

                if ((string)GridViewEstatistica.Rows[linha].Cells[1].Value == maisArea)
                {
                    GridViewEstatistica.Rows[linha].DefaultCellStyle = castanho;
                }

                if ((string)(GridViewEstatistica.Rows[linha].Cells[1].Value) == menosGini)
                {
                    GridViewEstatistica.Rows[linha].DefaultCellStyle = verde;
                }

                if ((string)GridViewEstatistica.Rows[linha].Cells[1].Value == maisGini)
                {
                    GridViewEstatistica.Rows[linha].DefaultCellStyle = amarelo;
                }

                GridViewEstatistica.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                linha++;
            }

         

        }

       
    }
}
