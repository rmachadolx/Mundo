﻿namespace Mundo
{
    using Servicos;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using System;
    using System.Threading.Tasks;
    using System.IO;
    using System.Linq;
    using System.Diagnostics;
    using System.Drawing;
    using ImageMagick;
    public partial class Form1 : Form
    {
        #region Atributos
        private List<Pais> Paises;
        private NetworkService networkService;
        private ApiService apiService;
        private DialogService dialogService;
        private DataService dataService;
        private Creditos creditos = new Creditos();
        private Estatistica estatistica;
        private MapaMundo mapaMundo;

        private Continente continente = new Continente();
        Pais pais = new Pais();
        bool load;


        #endregion

        #region Construtor
        public Form1()
        {
            InitializeComponent();

            apiService = new ApiService();
            dialogService = new DialogService();
            networkService = new NetworkService();
            dataService = new DataService();
            Paises = new List<Pais>();

            // chamar  o método assincrono que carrega os paises
            LoadPaises();


        }
        #endregion

        #region Metodos
        private async void LoadPaises()
        {

            //chamar o metodo que verifica se existe ligação à internet

            var connection = networkService.CheckConnection();

            if (!connection.IsSucess)
            {
                // se não existe  ligação à internet carrega base de dados local
                LoadLocalPaises();
                load = false;

            }
            else
            {
                // se existe  ligação à internet carrega base de dados da api 
                LabelResultado.Text = "A actualizar paises...";
                await LoadApiPaises();

                load = true;

            }
            if (Paises.Count == 0)
            {
                // se não existe  ligação à internet nem base de dados local; Primeira inicialização Requer  ligação à internet
                LabelResultado.Text = "Não há ligação à Internet" + Environment.NewLine +
                    "e não foram préviamente carregados os paises" + Environment.NewLine +
                    "Tente mais tarde!";

                LabelStatus.Text = "Primeira inicialização deverá ter ligação à internet";

                return;

            }
            else
            {
                CarregaComboBoxPaises();

            }

            CarregaComboBoxLinguas();

            if (load)
            {
                // se load = true  correu tudo bem ao carregar os paises pela internet
                LabelStatus.Text = String.Format("Paises carregados com sucesso da Internet em {0:F}", DateTime.Now);

            }
            else
            {
                // se não é poque foram carregados pela base de dado local

                // data de atualização da base de dados
                DateTime dataDeAtualizacao = File.GetLastWriteTime(@"Data\Paises.sqlite");
                // Mensggem a exibir que foi carregada pela base de dados na data indicada
                LabelStatus.Text = String.Format("Paíse carregados da base de dados, atulaizada em {0:F} ", dataDeAtualizacao);
                ProgressBar1.Maximum = Paises.Count();
                ProgressBar1.Value = Paises.Count();
            }








        }

        private void CarregaComboBoxLinguas()
        {
            ComboBoxLinguas.Items.Add("Português do Brasil");
            ComboBoxLinguas.ValueMember = "br";
            ComboBoxLinguas.Items.Add("Alemão");
            ComboBoxLinguas.ValueMember = "de";
            ComboBoxLinguas.Items.Add("Espanhol");
            ComboBoxLinguas.ValueMember = "es";
            ComboBoxLinguas.Items.Add("Árabe");
            ComboBoxLinguas.ValueMember = "fa";
            ComboBoxLinguas.Items.Add("Japonês");
            ComboBoxLinguas.ValueMember = "ja";
            ComboBoxLinguas.Items.Add("Françês");
            ComboBoxLinguas.ValueMember = "fr";
            ComboBoxLinguas.Items.Add("Italiano");
            ComboBoxLinguas.ValueMember = "it";
            ComboBoxLinguas.Items.Add("Croata");
            ComboBoxLinguas.ValueMember = "hr";
            ComboBoxLinguas.Items.Add("Holandês");
            ComboBoxLinguas.ValueMember = "nl";
        }

        private void CarregaComboBoxPaises()
        {
            ComboBoxPaises.DataSource = Paises;
            ComboBoxPaises.DisplayMember = "name";
        }

        private void LoadLocalPaises()
        {
            //carrega a lista Paises pela base de dados
            Paises = dataService.GetDataPaises();


        }

        private async Task LoadApiPaises()
        {

            //carrega a lista Paises pela api

            // variavel que recebe o resultado vindo da api 
            var response = await apiService.GetPaises("https://restcountries.eu", "rest/v2/all", ProgressBar1, LabelStatus);

            // colocaro resultado vindo da api  na lista Paises
            Paises = (List<Pais>)response.Result;

            //coloca a mensagem que foram carreagdos o paises
            LabelResultado.Text = response.Message;

            // apagar  abase de dados a fim de receber os paises atualizados
            dataService.DeleteData();

            // garavar na base de dados os paises atualizados chama-se o dataService passando a ProgressBar e LabalStatus 
            //para serem alteradas a partir de outro local fora do form1 onde estão definias 
            dataService.SaveData(Paises, ProgressBar1, LabelStatus);

            // exibindo o resultado
            LabelResultado.Text = String.Format("Foram carregadas {0} paises e respectivas bandeiras.", Paises.Count());




        }


        // a partir do pasi selecionado na ComboBoxPaises
        private void ComboBoxPaises_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ComboBoxPaises.SelectedIndex > -1)// obrigar a seleccionar um pais
            {
                ComboBoxLinguas.SelectedIndex = -1; //Tirar selecção da ComboBoxLinguas

                // receber no objecto pais a selecção da ComboBoxPaises fazendo cast do tipo  Pais; ou seja convertendo para objecto desse tipo
                pais = (Pais)ComboBoxPaises.SelectedItem;

                CarregaTreeView(pais, pais.translations.pt);


                try
                {
                    PictureBox1.Show(); // exibir PictureBox1

                    // caminho onde estão guardadas as imagens + nome + extensão(svg)
                    string path = @"Imagens\" + pais.name + ".svg";
                   
                    // Instanciar o Obecto nova do tipo MagickImage indicando o caminho para o svg a converter
                    MagickImage nova = new MagickImage(path);

                    //conerter para Bitmap 
                    Bitmap bandeira = nova.ToBitmap();

                    // carregar o bitmap na PictureBox1
                    PictureBox1.Image = bandeira;



                }
                catch (Exception ex)
                {
                    // em caso de erro

                    // ocultar PictureBox1
                    PictureBox1.Hide();

                    // utilizando o dialogService  para informar o utilizador houve erro com o nome do pais 
                    dialogService.DisplayMessage("Erro na imagem vinda da Api - Form1 ", ex.Message + " " + pais.name);


                }
            }
        }

        private void CarregaTreeView(Pais pais, string country)
        {
            TreeViewPaises.Nodes.Clear();
            TreeViewPaises.CollapseAll();



            TreeViewPaises.Nodes.Add("Pais: " + country);
            TreeViewPaises.Nodes.Add("Capital: " + pais.capital);
            TreeViewPaises.Nodes.Add("Região: " + pais.region);
            TreeViewPaises.Nodes.Add("População: " + String.Format("{0:N0} habitantes", pais.population));


            TreeViewPaises.Nodes.Add("Área: " + String.Format("{0:N0} km quadrados", pais.area));

            TreeViewPaises.Nodes.Add("Gini: " + String.Format("{0:N1} *", pais.gini));

            double densidade = Math.Round(Convert.ToDouble(Convert.ToDouble(pais.population) / pais.area), 1);
            TreeViewPaises.Nodes.Add("Densidade populacional: " + densidade.ToString());

            TreeViewPaises.Nodes.Add("Dominio de Topo");
            TreeViewPaises.Nodes.Add("Moeda");

            foreach (TreeNode principal in TreeViewPaises.Nodes)
            {
                if (principal.Text == "Dominio de Topo")
                {
                    foreach (var dominio in pais.topLevelDomain)
                    {
                        principal.Nodes.Add(dominio.ToString());

                    }
                }
                else if (principal.Text == "Moeda")
                {
                    foreach (var moeda in pais.currencies)
                    {
                        principal.Nodes.Add("Nome: " + moeda.name);
                        principal.Nodes.Add("Código: " + moeda.code);
                        principal.Nodes.Add("Simbolo: " + moeda.symbol);
                    }

                }

            }
        }


        private void ComboBoxLinguas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBoxLinguas.SelectedIndex != -1)
            {


                string nacao = "";
                switch (ComboBoxLinguas.SelectedItem.ToString())


                {
                    case "Português do Brasil":
                        nacao = pais.translations.br;
                        break;
                    case "Alemão":
                        nacao = pais.translations.de;
                        break;

                    case "Croata":
                        nacao = pais.translations.hr;
                        break;

                    case "Árabe":
                        nacao = pais.translations.fa;
                        break;

                    case "Espanhol":
                        nacao = pais.translations.es;
                        break;

                    case "Françês":
                        nacao = pais.translations.fr;
                        break;

                    case "Japonês":
                        nacao = pais.translations.ja;
                        break;

                    case "Holandês":
                        nacao = pais.translations.nl;
                        break;

                    case "Italiano":
                        nacao = pais.translations.it;
                        break;
                }

                CarregaTreeView(pais, nacao);
            }

        }






        // link para coeficiente Gini
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://pt.wikipedia.org/wiki/Coeficiente_de_Gini");
        }

        // Chama o form Continentes
        private void porContinenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Continente continente = new Continente();
            continente.Show();
        }

        // Chama o form MapaMundo
        private void verNoMapaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var connection = networkService.CheckConnection();
            if (!connection.IsSucess)
            {
                dialogService.DisplayMessage("Sem conecção à Internet", "Não tem conecção a internet,\n pelo que não será possivél apresentar-lhe o mapa !");

                return;

            }
            else
            {
                mapaMundo = new MapaMundo(Paises);
                mapaMundo.Show();
            }
        }

        // Chama o form Creditos
        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Creditos creditos = new Creditos();
            creditos.Show();

        }

        // Chama o form Estatisitica
        private void estatísticaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            estatistica = new Estatistica();
            estatistica.Show();
        }
        #endregion

        private void sobreToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Creditos creditos = new Creditos();
            creditos.Show();

        }
    }


}




